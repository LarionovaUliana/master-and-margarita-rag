from telegram import Update, InputFile
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
import os
from uuid import uuid4

from const import *


# Dictionary to store books for each user
user_books = {}
default_book_content = "Мастер и Маргарита"

with open('data.txt', 'r', encoding='utf-8') as f:
    book_content = f.read()
default_user_id = 'default'
user_books[default_user_id] = {}
user_books[default_user_id][default_book_content] = book_content

def start(update: Update, context: CallbackContext) -> None:
    """
    Handles the /start command. Sends a welcome message with instructions.
    
    :param update: Incoming update.
    :param context: Context of the callback.
    """
    update.message.reply_text(
        "Здравствуйте! Я бот, который отвечает на ваши вопросы о книге 'Мастер и Маргарита' или любой другой книге. "
        "Вы можете:\n"
        "- Задать вопрос о книге\n"
        "- /add_book - добавить новую книгу\n"
        "- /choose_book - выберите книгу, о которой вы хотите задать вопросы"
    )

def add_book(update: Update, context: CallbackContext) -> None:
    """
    Handles the /add_book command. Initiates the process of adding a new book.
    
    :param update: Incoming update.
    :param context: Context of the callback.
    """
    update.message.reply_text("Пожалуйста, введите название книги.")
    context.user_data['adding_book'] = True
    context.user_data['awaiting_book_title'] = True

def handle_add_book_title(update: Update, context: CallbackContext) -> None:
    """
    Handles the input of the book title when adding a new book.
    
    :param update: Incoming update.
    :param context: Context of the callback.
    """
    user_id = update.message.from_user.id
    if context.user_data.get('awaiting_book_title'):
        book_title = update.message.text
        context.user_data['new_book_title'] = book_title
        update.message.reply_text(f"Название книги '{book_title}' принимается. Пожалуйста, пришлите файл с книгой в формате .txt или .pdf")
        context.user_data['awaiting_book_title'] = False
        context.user_data['awaiting_book_file'] = True

def handle_document(update: Update, context: CallbackContext) -> None:
    """
    Handles the document upload. Saves the book content if the file is in .txt or .pdf format.
    
    :param update: Incoming update.
    :param context: Context of the callback.
    """
    user_id = update.message.from_user.id
    if context.user_data.get('awaiting_book_file'):
        document = update.message.document
        if document.mime_type == 'text/plain' or document.mime_type == 'application/pdf':
            file = document.get_file()
            file_path = f'{uuid4()}_{document.file_name}'
            file.download(file_path)
            with open(file_path, 'r', encoding='utf-8') as f:
                book_content = f.read()
            if user_id not in user_books:
                user_books[user_id] = {}
            book_title = context.user_data['new_book_title']
            user_books[user_id][book_title] = book_content
            update.message.reply_text(f"Книга '{book_title}' усешено добавлена!")
            os.remove(file_path)
            context.user_data['awaiting_book_file'] = False
            context.user_data['adding_book'] = False
        else:
            update.message.reply_text("Недопустимый формат файла. Пожалуйста, отправьте файл в формате .txt или .pdf.")
    else:
        update.message.reply_text("Пожалуйста, сначала введите название книги с помощью команды /add_book.")

def choose_book(update: Update, context: CallbackContext) -> None:
    """
    Handles the /choose_book command. Lists available books for the user to choose from.
    
    :param update: Incoming update.
    :param context: Context of the callback.
    """
    user_id = update.message.from_user.id
    if user_id in user_books and user_books[user_id]:
        book_list = "\n".join([f"{i+1}. {title}" for i, title in enumerate(list(user_books[default_user_id].keys()) + list(user_books[user_id].keys()))])
        update.message.reply_text(f"Выберите книгу, отправив ее номер:\n{book_list}")
        context.user_data['choosing_book'] = True
    else:
        update.message.reply_text("У вас нет добавленных книг. Используйте команду /add_book, чтобы добавить книгу. Сейчас вы можете задавать вопросы только о Мастере и Маргарите.")

def handle_choose_book(update: Update, context: CallbackContext) -> None:
    """
    Handles the user's book choice from the list of available books.
    
    :param update: Incoming update.
    :param context: Context of the callback.
    """
    user_id = update.message.from_user.id
    if context.user_data.get('choosing_book', False):
        try:
            book_index = int(update.message.text) - 1
            book_titles = list(user_books[default_user_id].keys()) + list(user_books[user_id].keys())
            if book_index == 0:
                selected_book_title = book_titles[book_index]
                context.user_data['selected_book'] = user_books[default_user_id][selected_book_title]
                update.message.reply_text(f"Книга '{selected_book_title}' выбрана")
                context.user_data['choosing_book'] = False
            elif 0 < book_index <= len(book_titles):
                selected_book_title = book_titles[book_index]
                context.user_data['selected_book'] = user_books[user_id][selected_book_title]
                update.message.reply_text(f"Книга '{selected_book_title}' выбрана")
                context.user_data['choosing_book'] = False
            else:
                update.message.reply_text("Неверный номер книги, попробуйте еще разз")
        except ValueError:
            update.message.reply_text("Пришлите номер книги")
    else:
        handle_message(update, context)

def handle_message(update: Update, context: CallbackContext) -> None:
    """
    Handles incoming text messages. Responds to questions about the selected book.
    
    :param update: Incoming update.
    :param context: Context of the callback.
    """
    user_id = update.message.from_user.id
    if context.user_data.get('adding_book', False):
        handle_add_book_title(update, context)
    else:
        user_message = update.message.text
        book_content = context.user_data.get('selected_book', default_book_content)
        prompt = f"Book: {book_content}\nQuestion: {user_message}\nAnswer:"
        # Uncomment and configure the following lines to integrate with OpenAI API
        """
        response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=prompt,
            max_tokens=150
        )
        bot_response = response.choices[0].text.strip()
        update.message.reply_text(bot_response)
        """

def main():
    """
    The main function to start the Telegram bot.
    """
    # Insert your Telegram API token
    updater = Updater(TELEGRAM_API_KEY, use_context=True)

    dispatcher = updater.dispatcher

    # Command handlers
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("add_book", add_book))
    dispatcher.add_handler(CommandHandler("choose_book", choose_book))

    # Message and document handlers
    dispatcher.add_handler(MessageHandler(Filters.document, handle_document))
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_choose_book))
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_message))

    # Start the bot
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
