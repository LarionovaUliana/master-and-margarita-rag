from elasticsearch import Elasticsearch, helpers
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.tokenize import word_tokenize
import string
import json

# Подготовка к нормализации текста
class TextNormalizer:
    def __init__(self):
        self.stop_words = set(stopwords.words('russian'))
        self.stemmer = PorterStemmer()
        self.lemmatizer = WordNetLemmatizer()
    
    def normalize(self, text):
        text = text.lower()
        text = text.translate(str.maketrans('', '', string.punctuation))
        words = word_tokenize(text)
        words = [word for word in words if word not in self.stop_words]
        words = [self.stemmer.stem(word) for word in words]
        words = [self.lemmatizer.lemmatize(word) for word in words]
        return ' '.join(words)

# Индексатор для загрузки и индексирования текстов в Elasticsearch
class BookIndexer:
    def __init__(self, es_host='http://localhost:9200', index_name='books'):
        #self.es = Elasticsearch([es_host])
        self.es = Elasticsearch("http://localhost:9200", basic_auth=("elastic", "elastic"), verify_certs=False)
        self.index_name = index_name
        self.normalizer = TextNormalizer()

    def load_text(self, file_path):
        encodings = ['utf-8', 'latin-1', 'windows-1252']
        for encoding in encodings:
            try:
                with open(file_path, 'r', encoding=encoding) as file:
                    return file.read()
            except UnicodeDecodeError:
                continue
        raise UnicodeDecodeError(f"Cannot decode file: {file_path}")

    def index_text(self, file_path, doc_id):
        text = self.load_text(file_path)
        normalized_text = self.normalizer.normalize(text)
        document = {
            'content': normalized_text
        }
        self.es.index(index=self.index_name, id=doc_id, body=document)
        
    def bulk_index(self, file_paths):
        actions = []
        for doc_id, file_path in enumerate(file_paths):
            text = self.load_text(file_path)
            normalized_text = self.normalizer.normalize(text)
            actions.append({
                '_index': self.index_name,
                '_id': doc_id,
                '_source': {'content': normalized_text}
            })
        helpers.bulk(self.es, actions)

# Поисковая система для обработки запросов и получения результатов из Elasticsearch
class SearchEngine:
    def __init__(self, es_host='http://localhost:9300', index_name='books'):
        self.es = Elasticsearch("http://localhost:9300", basic_auth=("elastic", "elastic"), verify_certs=False)
        self.index_name = index_name
        self.normalizer = TextNormalizer()
    
    def search(self, query):
        normalized_query = self.normalizer.normalize(query)
        search_body = {
            'query': {
                'match': {
                    'content': normalized_query
                }
            }
        }
        response = self.es.search(index=self.index_name, body=search_body)
        return response['hits']['hits']

# Пример использования
if __name__ == '__main__':
    # Создание индексатора и индексирование документов
    indexer = BookIndexer()
    book_paths = ['data.txt']  # пути к вашим файлам с книгами
    indexer.bulk_index(book_paths)

    # Создание поисковой системы и выполнение поиска
    search_engine = SearchEngine()
    query = "где был бал воланда"
    results = search_engine.search(query)
    
    for result in results:
        print(f"Document ID: {result['_id']}, Content: {result['_source']['content']}")
