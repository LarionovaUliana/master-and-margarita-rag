import re
import pandas as pd

class Splitter:
    """
    A class for splitting text into paragraphs and sentences.
    """

    def __split_paragraphs(self, text):
        """
        Splits the text into paragraphs, considering empty lines between them.
        If a paragraph contains less than two sentences, it is merged with the previous paragraph.

        :param text: The input text to be processed.
        :return: A list of processed paragraphs.
        """
        paragraphs = re.split(r'\n{2,}', text.strip())
        processed_paragraphs = []

        for paragraph in paragraphs:
            sentences = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', paragraph)
            if len(sentences) < 2:
                if processed_paragraphs:
                    processed_paragraphs[-1] += '\n\n' + paragraph.strip()
                else:
                    processed_paragraphs.append(paragraph.strip())
            else:
                processed_paragraphs.append(paragraph.strip())

        return processed_paragraphs

    def split_paragraphs_to_df(self, file_path):
        """
        Splits the text from a file into paragraphs and sentences, processes dialogues separately,
        and returns a DataFrame with the resulting text.

        :param file_path: The path to the text file.
        :return: DataFrame with split paragraphs and sentences.
        """
        with open(file_path, 'r', encoding='utf-8', errors='ignore') as file:
            text = file.read()

        paragraphs = self.__split_paragraphs(text)
        data = []

        for paragraph in paragraphs:
            sentences = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', paragraph)
            if len(sentences) <= 4:
                data.append(paragraph.strip())
            else:
                temp = []
                for sentence in sentences:
                    if len(temp) < 4:
                        temp.append(sentence.strip() + '.')
                    else:
                        data.append(' '.join(temp).strip())
                        temp = [sentence.strip() + '.']
                if temp:
                    data.append(' '.join(temp).strip())

        dialogue_data = self.__extract_dialogues(text)
        data.extend(dialogue_data)

        df = pd.DataFrame(data, columns=['Text'])
        return df

    def __split_sentences(self, text):
        """
        Splits the text into sentences using periods, question marks, and exclamation points.

        :param text: The input text to be split.
        :return: A list of sentences.
        """
        sentences = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?|!)\s', text)
        return sentences

    def split_sentences_to_pairs_df(self, text):
        """
        Splits the text into pairs of sentences and returns a DataFrame with these pairs.

        :param text: The input text to be split.
        :return: DataFrame with pairs of sentences.
        """
        sentences = self.__split_sentences(text)
        pairs = [f"{sentences[i]} {sentences[i+1]}" for i in range(len(sentences) - 1)]

        df = pd.DataFrame(pairs, columns=['Text'])
        return df

    def __extract_dialogues(self, text):
        """
        Extracts dialogues from the text, considering lines starting with a dash.

        :param text: The input text to be processed.
        :return: A list of dialogues.
        """
        dialogue_data = []
        buffer = []
        in_dialogue = False

        for line in text.splitlines():
            if line.startswith('—'):
                in_dialogue = True
                buffer.append(line.strip())
            elif in_dialogue and not line:
                in_dialogue = False
                dialogue_data.append(' '.join(buffer).strip())
                buffer = []
            elif in_dialogue:
                buffer.append(line.strip())

        if buffer:
            dialogue_data.append(' '.join(buffer).strip())

        return dialogue_data
