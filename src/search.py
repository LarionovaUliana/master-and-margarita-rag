
from splitter import Splitter
from const import *

class Searcher:
    def __init__(self):
        self.embeddings = OpenAIEmbeddings(api_key=OPENAI_API_KEY)
        self.splitter = Splitter()
        self.vectors = {}
        self.text = None

    def add_text(self, text):
        texts = self.splitter.split_text(text)
        self.vectors = FAISS.from_texts(texts, self.embeddings)